import sys
import argparse
from time import time
from functions import *
from termcolor import cprint
from collections import defaultdict


def prime_assembly(start, reads):
    """
    prime_assembly creates a consensus sequence to begin the assembly, using reads containing our start sequence.
    This function is time inefficient with large read dictionaries and is thus only used when not enough kmer prefixes
    could be matched to the start sequence.
    :param start: (String)      - A start sequence
    :param reads: (Dict)        - A hash table of reads with  key:    (Int)   - The identifier of the read
                                                              value:  (String)- The corresponding read sequence.
    :return: consensus (String) - A consensus sequence of all the reads that matched with our start sequence.
    """

    # ## Private functions. ##
    """These functions enables karp-rabin"s hashing algorithm."""
    def hasher(x):
        hash_table = (["A", "T", "C", "G"], [3, 5, 7, 11])
        return hash_table[1][hash_table[0].index(x)]

    def hash(seq): return sum(map(hasher, seq))

    def update_hash(hash_val, prev_n, next_n): return hash_val - hasher(prev_n) + hasher(next_n)
    # ## End private functions. ##

    r_len, s_len = len(reads[1]), len(start)
    s_hash = hash(start)
    indexes = []

    # for each read, check if it contains the start sequence (using a hash function if the user requested it).
    # if so, append a tuple of the read_id and the index at which the sequence matched to the indexes list.
    for read_id, read in reads.items():
        if len(indexes) >= 10:  # if we have 10 matches we carry on to the next step, for time"s sake.
            break
        if hash_it_requested:  # if the user requested a hash, use a karp_rabin algorithm to check for matching reads.
            for i in range(r_len-s_len+1):
                r_hash = hash(read[i:i+s_len]) if i == 0 else update_hash(r_hash, read[i-1], read[i+s_len-1])

                if r_hash == s_hash and start.find(read[i:i+s_len]) != -1:
                    indexes.append((read_id, i))
        else:   # else, use the very efficient str.start() method.
            index= read.find(start)
            indexes.append((read_id,index)) if index != -1 else None

    consensus = start
    # The maximum possible extension range is the length of a read, minus the smallest index on which a start sequence
    # could be found within our reads.
    max_index = r_len - min(idx for r_id, idx in indexes)
    # Create a consensus sequence nucleotide by nucleotide, at each step find the relevant nucleotides within
    # the matching reads. Pick the maximum occurring nucleotide and append it to the consensus sequence.
    # Do this until the sequencing depth is lower than desired (read_depth < min_seq_depth),
    # or if there is a tie between two nucleotides (len(new_nucl != 1).
    for i in range(max_index):
        nucleotides = defaultdict(lambda: 0)
        read_depth = 0
        for read_idx, s_index in indexes:

            if (s_index+s_len+i) < r_len:
                read = reads[read_idx]
                nucl = read[s_index+s_len+i]
                nucleotides[nucl] += 1
                read_depth += 1
        if read_depth < min_seq_depth:  # Stop the extension if the sequencing depth is below a treshold.
            return consensus
        else:
            # Find the maximum occurring nucleotide.
            new_nucl = [key for key, value in nucleotides.items() if value == max(nucleotides.values())]
            if len(new_nucl) != 1:    # Stop the extension if there"s a tie between two nucleotides.
                return consensus
            consensus += new_nucl[0]
    return consensus


def extend(S, kmers, len_S=0):
    """
        The core function of our program: "extend" searches for matching kmers on the current sequence and then
        recursively calls itself with a newly created consensus sequence until it finds a "stop" sequence inside it.
        :param S:       (String)    - The current consensus sequence.
        :param kmers:   (Dict)      - A dictionary of prefixes found throughout our collection of reads.
                                          key:    kmer (string)   - a kmer prefix found within our read collection.
                                          value:  indexes (list)  - list of indexes of the reads containing that kmer
        :param len_S:   (Int)       - The length of the previous consensus sequence. Ensures kmer matching is solely
                                      carried-out on the previous extension of S (S[len_S:len(S)]) and thus prevents
                                      cycling and redundant read matching.

        :return:        (Tuple)     - Containing :    S (string)      - The consensus sequence
                                                      success (bool)  - Whether or not a "stop" sequence is found in S
                                                      contigs (dict)  - The complete dictionary of the matched kmers and
                                                                        reads. Used to print-out the complete scaffold.
                                                                        See "print_scaffold" function.
    """
    contigs = defaultdict(lambda: [])

    if stop in S:
        return S, True, contigs

    elif len(S) > max_len:
        print("Fatal Error : Max length exceeded")
        sys.exit(1)

    # Get the matching reads and their respective indexes on S.
    matched_kmers, s_indexes = get_matching_reads(S, kmers, len_S)
    try:
        assert len(matched_kmers) != 0
    except AssertionError:
        print("Fatal Error: Could not match any reads to the consensus sequence.")
        sys.exit(1)

    # Call the function again with a newly created consensus sequence of S.
    res, success,contigs = extend(get_consensus(S, matched_kmers, s_indexes), kmers, len_S=len(S)-prefix_size)
    contigs.update(get_contig(matched_kmers, s_indexes))

    # If there is a "stop" in S: resolve the stack.
    if success:
        return res, True, contigs

    return "", False, None


def get_matching_reads(S, kmers, len_S):
    """
    Searches for matching kmer prefixes in the S[len_S:len(S)] range and returns the corresponding reads and the index
    at which it matched on S.
    :param S:       (String)    The current consensus sequence
    :param kmers:   (Dict)      A dictionary of prefixes found throughout our collection of reads.
                                        key:    kmer (string)   - a kmer prefix found within our read collection.
                                        value:  indexes (list)  - list of indexes of the reads containing that kmer
    :param len_S:   (Int)       The length of the previous consensus sequence. Ensures kmer matching is solely
                                carried-out on the previous extension of S (S[len_S:len(S)]) and thus prevents
                                cycling and redundant read matching.

    :return:        (Tuple)     Containing: matched_kmers (Dict) - A dictionary of matched prefixes.
                                                                         key:   kmer (string)   - a kmer prefix
                                                                         value: indexes (list)  - list of read_indexes
                                            s_indexes     (Dict) - A dictionary of matched prefixes.
                                                                         key:   kmer (string)   - a kmer prefix
                                                                         value: s_index (int)   - index of the match
    """

    # ### Private Functions. ###
    def check_diff(x, y):
        """
        Compare two strings and check if the number of differences is below a "max_error_rate" treshold.
        :params x, y: (String) The two strings to match.
        :return: (Bool) whether or not the differences between x and y are below the treshold.
        """
        return sum(1 for a, b in zip(x, y) if a != b) / min_overlap <= max_error_rate

    def check_overlap(S, indexes):
        """
        When there"s a matching kmer with S, check if the corresponding reads do indeed have a matching overlap with S
        Only check on a range of size "min_overlap". If the read does match: add it to the matched_kmers dict.
        :param S:       (String)    The current consensus sequence
        :param indexes: (List)      A list of indexes of the matching reads.
        :return:        (Void)
        """

        # extract the min_overlap* nucleotides after the matching kmer on S.
        suf_s = S[s_index + len_S + prefix_size:s_index + len_S + prefix_size + min_overlap]

        # For each read index, check if it is a reverse complement (index<0) and do the same.
        for index in indexes:
            if index < 0:
                suf_read = get_reverse_strand(reads[abs(index)][r_len - prefix_size - min_overlap:r_len - prefix_size])
            else:
                suf_read = reads[index][prefix_size:prefix_size + min_overlap]

            # If the two overlapping suffixes match: consider the current read as a candidate for extension.
            if check_diff(suf_s, suf_read):
                matched_kmers[kmer].append(index)
                s_indexes[kmer] = s_index + len_S

    # ## End Private Functions. ##
    matched_kmers = defaultdict(lambda: [])
    s_indexes = defaultdict(lambda: [])

    # For each kmer, check if it is found in S. If the user asked to check for the overlap (if check), then do so.
    # Else, directly append the corresponding reads as candidates for the next extension.
    for kmer, indexes in kmers.items():
        s_index = S[len_S:len(S)-min_overlap].find(kmer)

        if s_index != -1:   # If there is a match.
            if check_overlap_requested:
                check_overlap(S,indexes)
            else:
                matched_kmers[kmer] = indexes
                s_indexes[kmer] = s_index + len_S

    # s_indexes = {k: v for k, v in sorted(s_indexes.items(), key=lambda item:item[1])}
    return matched_kmers, s_indexes


def get_consensus(S, matched_kmers, s_indexes):
    """
    Takes a bunch of matching reads and extends the consensus sequence with them.
    :param S:               (String) - The current consensus sequence.
    :param matched_kmers:   (Dict)   - A dictionary of matched prefixes.
                                           key:   kmer   (String)- a kmer prefix
                                           value: indexes(List)  - list of read_indexes having this kmer_prefix
    :param s_indexes:       (Dict)   - A dictionary of matched prefixes.
                                           key:   kmer   (String)- a kmer prefix
                                           value: s_index(Int)   - index at which the match occurred on S
    :return: consensus      (String) - The next consensus sequence.
    """
    consensus = S
    # The maximum possible extension range is the length of a read, minus the offset between the length of S
    # and the furthest index on which a match occurred within S.
    max_extend = r_len - (len(S) - max(idx for idx in s_indexes.values()))

    # Extend the consensus sequence nucleotide by nucleotide, at each step find the relevant nucleotides within
    # the matching reads. Pick the maximum occurring nucleotide and append it to the consensus sequence.
    # Do this until the sequencing depth is lower than desired (read_depth < min_seq_depth),
    # or if there is a tie between two nucleotides (len(new_nucl != 1).
    for i in range(max_extend):
        read_depth = 0
        nucleotides = defaultdict(lambda: 0)

        for kmer, read_indexes in matched_kmers.items():
            s_index = s_indexes[kmer]

            for r_index in read_indexes:
                if (len(S) - s_index + i) < r_len:    # Is this read still overlapping at this nucleotide position?
                    read = get_reverse_strand(reads[abs(r_index)]) if r_index < 0 else reads[r_index]
                    nucl = read[len(S) - s_index + i]
                    nucleotides[nucl] += 1
                    read_depth+=1

        if read_depth < min_seq_depth:  # Stop the extension if the sequencing depth is below a treshold.
            return consensus
        else:
            # Find the maximum occurring nucleotide.
            new_nucl = [key for key, value in nucleotides.items() if value == max(nucleotides.values())]
            if len(new_nucl)!=1:    #Stop the extension if there"s a tie between two nucleotides.
                return consensus
            consensus += new_nucl[0]
    return consensus


if __name__ == "__main__":
    # Parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="""\
            ========================================================

                                OLC read assembler

            ========================================================""")
    # Mandatory arguments
    parser.add_argument("-r", "--reads-file", dest="reads_file", required=True,
                        help="Input file containing reads to assemble")

    parser.add_argument("-s", "--start-stop", dest="start_stop_path", required=True,
                        help="Input fasta file with the start and stop sequences")

    # Optional arguments
    parser.add_argument("-f", "--output_file", dest="output_file", default="assembled_sequence.fasta",
                        help="Name of the output fasta that will contain the assembled sequence")

    parser.add_argument("-p", "--prefix-size", dest="prefix_size", default=30,
                        type=int, help="Length of indexed prefixes")

    parser.add_argument("-l", "--max-length", dest="max_length", default=10000,
                        type=int, help="Max length of the assembly. The assembly will stop if this length is exceeded")

    parser.add_argument("-o", "--min-overlap", dest="min_overlap", default=10,
                        type=int, help="The minimum required overlap between a read and the current consensus.")

    parser.add_argument("-c", "--check-overlap", dest="check_overlap", default=False, action="store_true",
                        help="Check if the read has a consistent overlap with the current consensus.\
                                            Will accept a default error rate of 10% - see max-error-rate")

    parser.add_argument("-e", "--max-error-rate", dest="max_error_rate", default=0.1,
                        type=int, help="Ratio of allowed substitutions when checking for read/consensus overlap")

    parser.add_argument("-m", "--min-seq-depth", dest="min_seq_depth", default=3,
                        type=int, help="Minimum reads overlapping required to continue the assembly. The assembly will stop\
                if there are less reads found than the value given")

    parser.add_argument("-k", "--karp-rabin", dest="karp_rabin", default=False, action="store_true",
                        help="Apply Karp-Rabin's hashing algorithm when priming the sequence")

    parser.add_argument("-g", "--generate-assembly", dest="generate_assembly", default=False, action="store_true",
                        help="Boolean indicating if you want to save the full assembly in a .ANSI file")

    parser.add_argument("-a", "--assembly", dest="assembly", default="assembly.ANSI",
                        type=str, help="File where the full assembly will be written")


    # Show help message if no arguments are passed
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    # Save user arguments in a dictionnary
    args = parser.parse_args()
    args = vars(args)

    # Save arguments and keep them as global parameters.
    output_file = args["output_file"]
    prefix_size = args["prefix_size"]
    hash_it_requested = args["karp_rabin"]
    max_len = args["max_length"]
    check_overlap_requested = args["check_overlap"]

    min_overlap = args["min_overlap"]
    max_error_rate = args["max_error_rate"]
    min_seq_depth = args["min_seq_depth"]
    generate_assembly = args["generate_assembly"]
    assembly_file = args["assembly"]


    s_time = time()
    # Preprocess the data
    reads = get_reads(args["reads_file"])
    start, stop = get_start_stop(args["start_stop_path"])
    kmers = index_reads(reads, prefix_size=prefix_size)
    r_len = len(reads[1])  # Sample the length of a read (we can, since there are no indels...)
    min_overlap = min_overlap if check_overlap_requested else 0  # Disable overlap_check if the user didn't request it.

    #Run the assembly
    print("Preliminary checks...")
    try:
        matching_reads, s_indexes = get_matching_reads(start, kmers, len_S=0)  # start a 'dry run' with 'start'
        assert len(matching_reads) >= 3                                        # check if seq-depth is satisfactory
    except AssertionError:                                                     # prime the assembly if not...
        print("Not enough reads... Priming assembly...")
        primer = prime_assembly(start, reads)
        print("Starting assembly...")
        sequence, great_success, contigs = extend(primer, kmers, len_S=prefix_size)
    else:
        print("Starting assembly...")
        sequence, great_success, contigs = extend(start, kmers, len_S=0)
    finally:
        if great_success:
            write_fasta(sequence, output_file)
            print_scaffold(sequence, contigs, reads, r_len, assembly_file) if generate_assembly else None
            cprint(f'Success! The sequence can be found in {output_file}','green')
        else:
            raise Exception("Something went terribly wrong! Sorry...")

        e_time = time()
        cprint(f'Exec Time: {round(e_time - s_time,3)} seconds.','blue')

    # TODO: -------------------------------------------- MANDATORY -----------------------------------------------------
    #       - Done!
    #       -------------------------------------------- IMPORTANT -----------------------------------------------------
    #       - get_matching_reads:   Use reverse complements only when sequencing depth is too low
    #       -------------------------------------------- OPTIONNAL -----------------------------------------------------
    #       - assembly          :   Find all possible sequences
    #                               => Call S recursively for each possible kmer position
    #       - print_scaffold    :   Find another file format that handles colors
    #       - Spaghetti_Code.py :   Create a Databunch object to send variables instead of using global parameters
    #       - Bad_Practice.py   :   Resolve variable names shadowing function input-parameters.

