# GAP Filling OLC assembler

## Overview

The assembler is based on the Overlap Layout Consensus genome assembly algorithm (OLC), meaning it finds overlaps between reads and generates a sequence based on the prevalence of the nucleotides found within reads for each position. The assembler works recursively to find indexed kmers matching at the end of the sequence that is being assembled on the fly.

The assembler is able to account for errors thanks to the consensus made between reads, but also by allowing the mapping of reads with some errors thanks to the `--max-error-rate` argument. Different arguments allow to fine tune the algorithm's behavior, such as the length of the kmer-prefixes, the number of reads required to extend the consensus, etc.

### Context

This student project was carried as part of the 'Master Bioinformatique, Université Rennes1' and as an evaluation for the 'Algorithms for Biological Sequence Analysis' teaching module, under the supervision of Research Associates [Claire Lemaitre](http://people.rennes.inria.fr/Claire.Lemaitre/) and [Pierre Peterlongo](http://people.rennes.inria.fr/Pierre.Peterlongo/) -  ([IRISA-INRIA, GenScale Team](https://team.inria.fr/genscale/))

## Requirements

Python > 3.6 is required due to the use of f-strings in the code.

No external libraries are needed to run the assembler.

## Installation

### Windows/Linux/MacOS

The easiest way to install the assembler is by using `git clone` in a CLI (Command Line Interface)
`git clone https://gitlab.com/SwannM/gap-filling-olc.git`

You can also manually download an archive of the assembler directly on the [git repository](https://gitlab.com/SwannM/gap-filling-olc) page, then to decompress the archive to get the repository.

### Windows

**Note** : You need to use `Powershell` to run the following command lines for Windows.

Create a new WebClient object :

```powershell
$client = new-object System.Net.WebClient
```

Download the archive using the `DownloadFile` method :

```powershell
$client.DownloadFile("https://gitlab.com/SwannM/gap-filling-olc/-/archive/master/gap-filling-olc-master.zip","./gap-filling-olc-master.zip")
```

Unzip the archive with the `Expand-Archive` cmdlet :

```powershell
Expand-Archive -LiteralPath gap-filling-olc-master.zip gap-filling-olc-master
```

### Linux

Use `wget` to retrieve the archive :

```bash
wget https://gitlab.com/SwannM/gap-filling-olc/-/archive/master/gap-filling-olc-master.tar.gz --output-document gap-filling-olc.tar.gz
```

Uncompress the archive using `tar` :

```bash
tar -xvzf gap-filling-olc-master.tar.gz
```

### MacOS

Use `curl` to retrieve the archive :

```bash
curl https://gitlab.com/SwannM/gap-filling-olc/-/archive/master/gap-filling-olc-master.tar.gz --output gap-filling-olc-master.tar.gz
```

Uncompress the archive using `tar` :

```bash
tar -xvzf gap-filling-olc-master.tar.gz
```

## Usage

### Quickstart

The assembly can be run using default parameters with `python assemble.py --read-file READ_FILE --start-stop START_STOP_FILE`

Only the `--read-file` and `--start-stop` arguments are mandatory.

If you do not specify an output file for the generated sequence using `--output-file`, it will be written in `assembled_sequence.fasta` in your current directory  by default.

You can use `python assemble.py --help` to show a list of all available arguments in a shell.

### Inputs

* A file (fastq/fasta) containing the reads to assemble. Must be passed to the program using the `--read-file` argument.
* A file with the start and stop sequences. Must be passed using the `--start-stop` argument.

### Outputs

* A fasta file with the assembled sequence : `assembled_sequence.fasta`. The output file name can be changed with the `--output-file` argument.
* [Optional] : An ANSI file with the consensus sequence matched against all the reads used for the assembly : `assembly.ANSI` if the `--generate-assembly` parameter is passed. The name of the output file can be changed using the `--assembly` parameter. For more information, see [Visualizing the assembly](#visualizing-the-assembly) for more information.

### Arguments list

| Flag | Argument            | Information                                                                                                                                                         | Default value            |
| ---- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| -r   | --read-file         | `.fasta` input file containing the reads to assemble                                                                                                                | N/A                      |
| -s   | --start-stop        | `.fasta` input file with the start and stop sequences                                                                                                               | N/A                      |
| -f   | --output-file       | Name of the output fasta file that will contain the assembled sequence                                                                                              | assembled_sequence.fasta |
| -p   | --prefix-size       | Length of indexed prefixes                                                                                                                                          | 30                       |
| -l   | --max-length        | Max length of the assembly (**in bases**). The assembly will stop if this length is exceeded                                                                        | 10000                    |
| -o   | --min-overlap       | Minimum required overlap between a read and the current consensus                                                                                                   | 10                       |
| -c   | --check-overlap     | Check if the read has a consistent overlap with the current consensus. See `--max-error-rate`                                                                       | False                    |
| -e   | --max-error-rate    | Ratio of allowed substitutions when checking for read/consensus overlap. Only used when `--check-overlap` is set to `True`                                          | 0.1                      |
| -m   | --min-seq-depth     | Minimum reads overlapping required to continue the assembly. The assembly will stop if there are less reads found than the given value                              | 3                        |
| -k   | --karp-rabin        | Apply Karp-Rabin's hashing algorithm when priming the sequence                                                                                                      | False                    |
| -g   | --generate-assembly | Use if you want to save the full assembly in a file                                                                                                                 | False                    |
| -a   | --assembly          | File where the full assembly will be written, showing each read and its position in the assembled sequence. File generated only if `--generate-assembly` is passed. | assembly.ANSI            |
| -h   | --help              | Show a help message with all the parameters available.                                                                                                              | N/A                      |

### Visualizing the assembly

It is possible to view the consensus sequence and all the reads used for the assembly by adding the `--generate-assembly` parameter. Enabling this parameter will by default save the assembly in a file named `assembly.ANSI` that you can rename using the `--assembly` parameter.

The ANSI format allows the use of colors to easily distinguish **reverse** reads from their **forward** counterpart.

ANSI files can be viewed using different IDE packages such as [ANSIescape](https://github.com/aziz/SublimeANSI) for **Sublime Text 3** or [language-ansi-styles](https://atom.io/packages/language-ansi-styles) for **ATOM**. Be aware that automatic word-wrapping must be disabled from your text editor to appropriately display the complete assembly.

* Pipes are placed every 10 nucleotides to serve as a ruler.
* The consensus sequence appears in **blue** at the top.
* Reverse reads are marked in **green** while forward reads appear in white.

![Assembly overview](https://gitlab.com/SwannM/gap-filling-olc/raw/master/pictures/sample_whole_assembly.png "Assembly overview")

### Possible errors

The `--max-length` parameter is used to prevent the assembly from infinitely looping in the case of a highly repetitive genome or region. If this value is exceeded, the following message will be printed `Fatal Error : Max length exceeded` and the program will exit.

When no valid matches are to be found for the assembled sequence, the program will print `Fatal Error: Could not match any reads to the consensus sequence` and exit.

## Results

### Overall Benchmarking

Benchmarks for **execution time** and **memory-usage**  during the assembly were run with *E. coli* reads of different size. Average execution time and memory usage calculations were performed 5 times and meaned, using the native `time.timeit()` and [guppy3](https://github.com/zhuyifei1999/guppy3) modules, respectively.

The length of the reference query sequence was of 1 kb for the 2kb and 10kb reads dataset, and of 10kb for the subsequent ones (100kb to 5000).

| Reads size | Error percentage | Execution time | Memory Usage (bytes) | Functional? | Correct Sequence? |
| ---------- | ---------------- | -------------- | -------------------- | ----------- | ----------------- |
| **2kb**    | Perfect          | ~0.03s         | 10823921    10.82Mb  | Yes         | Yes               |
|            | 0.1%             | ~0.11s         | 10835060    10.83Mb  | Yes         | Yes               |
|            | 1%               | ~0.06s         | 10825806    10.82Mb  | Yes         | Yes               |
| **10kb**   | Perfect          | ~1.20s         | 12029348    12.03Mb  | Yes         | Yes               |
|            | 0.1%             | ~1.20s         | 12017979    12.02Mb  | Yes         | Yes               |
|            | 1%               | ~1.20s         | 11956638    11.96Mb  | Yes         | Yes               |
| **100kb**  | Perfect          | ~4.38s         | 21353082    21.35Mb  | Yes         | Yes               |
|            | 0.1%             | ~4.18s         | 21342418    21.34Mb  | Yes         | Yes               |
|            | 1%               | ~4.31s         | 21420711    21.42Mb  | Yes         | Yes               |
| **1000kb** | Perfect          | ~32.7s         | 115505643   115.5Mb  | Yes         | Yes               |
|            | 0.1%             | ~35.2s         | 115804069   118.8Mb  | Yes         | Yes               |
|            | 1%               | ~33.1s         | 128432174   128.4Mb  | Yes         | Yes               |
| **5000kb** | Perfect          | ~110s          | 444466497   444.5Mb  | Yes         | Yes               |
|            | 0.1%             | ~110s          | 448940066   448.9Mb  | Yes         | Yes               |
|            | 1%               | ~110s          | 481681081   481.7Mb  | Yes         | Yes               |

### Performance and scalability

#### Execution time

The costliest phase of our assembler in regards to time complexity is the `get_matching_reads()` function, which carries-out a thorough search to find matching reads candidates for the next extension of our consensus sequence S. A naïve approach would be to directly try to match prefixes of variable length to S for each individual read, which would result in a time complexity of <img src="https://render.githubusercontent.com/render/math?math=O(n^{2}\cdot m)"> , with *n* being the overall number of reads, and *m*, the read length.  Such time-scaling would be less than desirable when considering the high number of reads one would have to handle for an entire genome. Heuristics were therefore applied to mitigate this issue:

* A prefix of constant size for each read is matched against S at each step: our entire reads dataset can therefore be entirely encapsulated within a hash table and finding matching candidates can be done using a prefix dictionary which matches occurring prefixes to their corresponding reads using an identifier
* Once matching reads candidates have been found, the subsequent overlap between a read and the consensus sequence (meaning from the end of the prefix to the end of S) is solely checked for a set number of nucleotides. Likewise, with a sufficient prefix-size, this feature is redundant with our error correction function and is therefore disabled by default. See: [Sequencing errors](#Sequencing-error-correction-and-results-quality) section below.

These two heuristics enable us to attain a time-complexity of <img src="https://render.githubusercontent.com/render/math?math=O(k)">, with *k* being the length of our kmers prefixes dictionary. Since our dictionary contains both the original prefixes and the prefixes of the reverse-complement of our reads, a worst case scenario would correspond to a size of <img src="https://render.githubusercontent.com/render/math?math=k=2n">. However, we have found that the size of our dictionary grossely corresponds to the original number of reads.

A second cost-ineffective -but optional- step of our assembler is when priming the consensus sequence using the `prime_assembly()` function. This function solely activates during pre-processing if the assembler was unable to create a robust enough extension when directly using the raw 'start' sequence provided by the user. When such is the case, `prime_assembly()` directly searches for reads containing the start sequence to create a larger consensus sequence. This results in a time-complexity of <img src="https://render.githubusercontent.com/render/math?math=O(n)">, which could potentially double the runtime of the overall algorithm. To mitigate this, `prime_assembly()` only searches for a maximum of 10 matching reads, which is enough to sufficiently extend the consensus sequence. With a prefix size of k=30, this process is practically bound to be set in motion, but can be avoided by manually decreasing the prefix size using the `--prefix-size` argument, or by providing a larger 'start' sequence (when available).

Likewise, the `print_contig()` function has a time complexity of <img src="https://render.githubusercontent.com/render/math?math=O(n\cdot log(n))">, with *n* being the number of reads matched. This can become quite burdenful regarding the efficiency of our overall algorithm (~14.6s for the 5000kb dataset ). For this reason, this function is disabled by default.

#### Memory Usage

The use of a kmer-prefix dictionary and a hash-table for reads allows the overall RAM usage to scale linearly with the number of reads.

### Sequencing error correction and results quality

Our assembler handles error-correction on the fly while extending the consensus sequence, using the `get_consensus()` function. `get_consensus()` extends the consensus sequence base by base (in the 5' --> 3' direction) by selecting the highest occurring nucleotide within the previously matched reads. This extension continues until one of the two following event occurs:

* The sequencing depth reaches a lower bound that is considered insufficient to 'safely' pursue the extension. This threshold is set at 3 by default, but can be user-defined using the optional `--min-seq-depth` argument.
* The algorithm encounters two equally represented nucleotides when searching for the highest occurring one: the extension is cut short, as it simply has a 50% chance to pick the wrong one.

By default, the assembler does not check for any overlap consistency between the consensus sequence and candidate reads (aside from the prefix). This heuristic is made possible by selecting a robust enough prefix size (k=30, by default). Nonetheless, this feature is still available to the user by adding the `--check-overlap` argument.

If such is the case, the assembler will check for overlap consistency before retaining the read as a valuable candidate for extension. This overlap-checking is performed on a set number of nucleotides located downstream of the previously matched prefix (default=10), while accounting for possible sequencing errors (default=0.1). This will moreover exclude any read that does not sufficiently overlap with the consensus sequence. The minimum-allowed overlap and maximum allowed error-rate can both be user defined using the `--min-overlap` and `--max-error-rate`, respectively.

As shown in the above table (see: [Benchmarking](#overall-benchmarking)), the results yielded by the assembler are consistent. Each test done with reads of different size ran without returning any error, and the sequence returned complies with the known *E. coli* Open-Reading-Frame.

## Discussion

### Upsides

For anyone looking to sequence a **specific** region of the genome, the  current conception of our OLC genome assembly algorithm may provides a good balance between accuracy of the result and time/memory efficiency. though it prioritizes on the former.

Several points allow the generated assembly to be consistent :

* The default `prefix-size` of 30 is significant enough to reduce the risk of discrepant read matching due to repetitive sequence.
* The consensus handles errors that could occur in reads matching the sequence being assembled.
* The `min-seq-depth` parameter makes sure that there are enough bases for each position to have an accurate consensus.

### Downsides

The most glaring weakness of our architecture is that the `get_consensus()` function cannot account for possible indels at this state. This characteristic implies *verbatim* that, while our assembler could potentially cope with Generation Sequencing methods such as ***Illumina*** or ***SOLiD*** (which are characterized by substitutions as their dominant error-type), it could hardly be relevant in the field if used with long-read sequencers (***IonTorrent***, ***PacBio***, ***ONT***).

Our assembler can solely work on haploid organisms, since our `get_consensus()` method is designed to interrupt extension as soon as two nucleotides are found with an equal occurence, meaning it would terminate as soon as it encouters an heterozygous SNP in a diploïd organism.

Finally, a major downside of our current assembler is that it does not account for alternative splicing, but only returns the first consensus sequence found, while multiple relevant sequences could be possible in reality. In other terms, our OLC is not up to the task of providing insights regarding different possible isoforms of an ORF.

### Optimization

At the current state, reverse-complement kmer prefixes are systematicaly available to our `get_matching_reads()` function, while forward reads could very-well provide sufficient coverage by themselves. An update could be done to tap-into reverse kmers prefixes only when there weren't enough matched forward reads to continue the assembly.

## Authors

Maël Lefeuvre : mael.lefeuvre.1@etudiant.univ-rennes1.fr

Swann Meyer : swann.meyer@etudiant.univ-rennes1.fr
