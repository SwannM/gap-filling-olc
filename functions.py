import os.path
from collections import defaultdict


def get_reverse_strand(seq):
    """
    get_reverse_strands returns the reverse complement of a DNA sequence
    :param seq: (String) a DNA sequence.
    :return: (String) the reverse complement of "seq".
    """
    pairs = {"A": "T", "T": "A", "C": "G", "G": "C"}
    return "".join([pairs[nucleo] for nucleo in seq][::-1])


def get_reads(fastq):
    """
    get_reads generates a dictionary storing individual reads from a single fastq file.
    :param fastq:   (String)    - A filepath to the fastq file containing the necessary reads.
    :return: reads  (Dict)      - A dictionary of reads with    key:    (Int)   - the identifier of the read
                                                                value:  (String)- the sequence of the read.
    """
    print("Retrieving reads from fastq file...")
    reads = {}
    with open(fastq, "r") as file:
        i = 0
        for line in file.readlines():
            if line.startswith(">"):    # If the line starts with a ">" then the next line is the actual sequence.
                i += 1
            else:
                reads[i] = line.strip("\n")
    return reads


def get_start_stop(fasta):
    """
    Return a tuple (start, stop) from a fasta file with 2 sequences whose names include start and stop,
    respectively for the first and second seq.
    :param fasta:   (String)- A filepath to the fasta file containing the start and stop sequences.
    :return:        (Tuple) - Containing: start (String) - the start sequence.
                                          stop  (String) - the stop sequence.
    """

    print("Retrieving start and stop sequences...")
    if not fasta.endswith(".fasta") and not fasta.endswith(".fa"):
        raise Exception("Please pass in a fasta file")

    start, stop = "", ""

    with open(fasta, "r") as file:
        f = iter(file.readlines())

        for line in f:
            if line.startswith(">") and "start" in line.lower():
                start = next(f).strip("\n")
            elif line.startswith(">") and "stop" in line.lower():
                stop = next(f).strip("\n")

        if not start:
            raise Exception("No start k-mer found")
        if not stop:
            raise Exception("No stop k-mer found")
    return start, stop


def index_reads(reads, prefix_size=10):
    """
    index_reads generates a dictionary of non-redundant kmer prefixes found within the reads.
    :param reads:       (Dict)  - A dictionary of reads with key:   (Int)   - the identifier of the read
                                                             value: (String)- the sequence of the read.
    :param prefix_size: (Int)   - The required size of the kmer to search.
    :return: kmers      (Dict)  - A dictionary of k-mers with key:   (String)- a k-mer sequence.
                                                              value: (List)  - a list of reads identifiers having
                                                                               such kmer as a prefix
    """
    kmers = defaultdict(lambda: [])
    read_len = len(reads[1])
    for read_id, read in reads.items():                        # For each read...
        kmers[read[0:prefix_size]].append(read_id)             # Store the prefix of the read + its identifier.
        rev = get_reverse_strand(read[read_len-prefix_size:])  # Store the prefix of the reverse complement of that read
        kmers[rev].append(-read_id)                            # Rev => store the negative value of its identifier.
    return kmers


def get_contig(matched_kmers, s_indexes):
    """
    get_contigs converts two dictionaries of matches into a single one.
    This will help to generate a full history of our sequence extension once it is done, and allow us to print a
    complete scaffold of our sequence extension.
    :param matched_kmers:   (Dict) - A temporary dictionary of matched prefixes at a given recursion of extend.
                                          key:   kmer       (String)    - a kmer prefix
                                          value: indexes    (List)      - list of read_indexes
    :param s_indexes:       (Dict) - Indexes on which our kmers matched at a given recursion of extend.
                                          key:   kmer       (String)    - a kmer prefix
                                          value: s_index    (Int)       - index of the match on the consensus sequence.
    :return: contig_indexes (Dict) - A dictionary of matched prefixes:
                                          key:   s_index    (Int)       - index of the match on the consensus sequence.
                                          value: read_index (List)      - list of read_indexes
    """
    contig_indexes = defaultdict(lambda: [])

    for kmer, s_index in s_indexes.items():
        read_indexes = matched_kmers[kmer]
        contig_indexes[s_index] += read_indexes

    return contig_indexes


def find_contig(idx, indexes, r_len,read_list=None):
    """
    find_contig recursively finds a list of non-overlapping reads throughout the "history" of our matched reads.
    :param idx:         (Int)   - The index at which the current read matched on the consensus sequence.
    :param r_len:       (Int)   - The length of a read.
    :param indexes:     (List)  - A list of tuples of form (s_index, read_id) == the complete history of our matched reads
    :param read_list:   (List)  - A list of tuples of non-overlapping reads.
    :return: read_list  (List)  - A list of tuples of non-overlapping reads.
    """
    read_list = [] if read_list is None else read_list

    # Find the next non-overlapping read
    res = next(((s_idx, r_idx) for s_idx, r_idx in indexes if s_idx > idx + r_len), None)
    if res is None:     # If there is no such read, then the line is complete. return our list of reads for that line.
        return read_list
    else:               # Else, append the read to the list and recursively find the next non-overlapping read.
        read_list.append(res)
        return find_contig(res[0], indexes, r_len, read_list)


def print_scaffold(sequence, contigs, reads,  r_len, file_name):
    """
    print_scaffold generates a .ANSI file displaying all the reads that have been used for the assembly
    in their respective context and their position in the scaffold.
    :param sequence:    (String)    - The full consensus sequence
    :param contigs:     (Dict)      - A dictionary containing the complete history of matched kmers for that sequence.
                                          key:   s_index (Int)  - the index on which reads matched on the consensus seq.
                                          value: read_id (List) - a list of read identifiers.
    :param reads:       (Dict)      - A dictionary of reads with
                                          key:    (Int)   - the identifier of the read
                                          value:  (String)- the sequence of the read.
    :param file_name:   (String)    - The output .ANSI filepath.
    :param r_len:       (Int)       - The length of a read.
    :return: (Void)
    """
    print(f"Writing full assembly into {file_name}...")

    # Generate a redundant list of tuples with the contigs dictionary, of the form (s_index, read_id) for each read_id
    indexes = []
    for s_index, read_indexes in contigs.items():
        [indexes.append((s_index, read_idx)) for read_idx in read_indexes]

    indexes = sorted(((k, v) for k, v in indexes), key=lambda tup: tup[0])  # Sort the list by order of s_index
    lines = []

    # Each line of our file should contain reads separated by more than their own length. We thus need to recursively
    # find the first read which does not overlap with its predecessor.
    while indexes is not None:
        couple = indexes[0]
        # Recursively find a list of non overlapping predecessors
        line = find_contig(idx=couple[0], indexes=indexes, r_len=r_len, read_list=None)
        line.insert(0, couple)  # add the "root-read" at the start of our line.
        lines.append(line)

        # TODO : Deleting elements of a list while looping through them is a very bad practice.
        #        A workaround should be found
        indexes = [x for x in indexes if x not in line]  # Delete the reads which were used for our line.
        if not indexes:   # Stop when our list of reads is empty.
            lines[-1].pop(0)
            break

    output = ""
    for line in lines:
        for i in range(len(line)):
            s_idx, r_idx = line[i]
            if r_idx < 0:  # Get the reverse complement of the read if needed.
                read = get_reverse_strand(reads[abs(r_idx)]) if r_idx < 0 else reads[r_idx]
                read = f"\033[92m{read}\033[0m"  # Give him a green tint in this case.
            else:
                read = reads[r_idx]

            # Calculate the number of blank characters separating the current read from its predecessor
            step = s_idx if i == 0 else s_idx - line[i - 1][0] - r_len
            output += " "*step+read

        output += "\n"

    with open(file_name, "w+") as f:
        f.write((" " * 9 + "|") * (len(sequence) // 10)+"\n")
        f.write("\033[94m" + sequence + "\033[0m"+"\n")
        f.write(output)


def write_fasta(seq, file):
    """
    write_fasta creates a fasta file containing the input consensus sequence.
    :param seq:     (String) - the consensus sequence
    :param file:    (String) - Filepath of the output fasta file.
    :return: (Void)
    """
    print(f"Writing consensus sequence in {file}...")

    if not file.endswith(".fasta") and not file.endswith(".fa"):
        file += ".fasta"

    with open(file, "a") as f:
        if file.endswith(".fasta"):
            header = file.replace(".fasta","")
        elif file.endswith(".fa"):
            header = file.replace(".fa","")

        f.write(f">{header}\n{seq}\n")
